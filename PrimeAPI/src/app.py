from flask import Flask, jsonify, request  
from flask_sqlalchemy import SQLAlchemy  #ORM
from flask_marshmallow import Marshmallow  #definir Schemas
import datetime as date
from flask_cors import CORS

app = Flask(__name__)

#Database
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:PruebA01@localhost/primeStone'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
CORS(app, resources={r"/*": {"origins": "*"}})

db = SQLAlchemy(app)
ma = Marshmallow(app) 

#models
class Rol(db.Model):
     IdRol = db.Column(db.Integer, primary_key=True)
     Nombre = db.Column(db.String)
     Descripcion = db.Column(db.String)
     FechaRegistro = db.Column(db.DateTime)
     Activo = db.Column(db.String)
     
     def __init__(self, Nombre, Descripcion):       
        self.Nombre = Nombre
        self.Descripcion = Descripcion        
        self.FechaRegistro = (date.datetime.now()).strftime("%Y-%m-%d")
        self.Activo = '1' 

class Cliente(db.Model):
     IdCliente = db.Column(db.Integer, primary_key=True)
     Identificacion = db.Column(db.String)
     TipoIdentificacion = db.Column(db.String)      
     Nombre = db.Column(db.String)
     RazonSocial = db.Column(db.String)
     Direccion = db.Column(db.String)
     Telefono = db.Column(db.String)
     Email = db.Column(db.String)     
     Activo = db.Column(db.String)   
     FechaRegistro = db.Column(db.DateTime)
     IdSocket = db.Column(db.Integer)
     IdRol = db.Column(db.Integer)

     def __init__(self,Id,TId,Nombre,RZ,Dir,Tel,Email,IdSocket,IdRol):       
        self.Identificacion = Id
        self.TipoIdentificacion = TId  
        self.Nombre = Nombre
        self.RazonSocial = RZ
        self.Direccion = Dir
        self.Telefono = Tel  
        self.Email = Email
        self.IdSocket = IdSocket         
        self.IdRol = IdRol          
        self.FechaRegistro = (date.datetime.now()).strftime("%Y-%m-%d")
        self.Activo = '1'  

class Medidor(db.Model):
     IdMedidor = db.Column(db.Integer, primary_key=True)
     Nombre = db.Column(db.String)
     Descripcion = db.Column(db.String)
     Peso = db.Column(db.String)
     Dimensiones = db.Column(db.String)
     TMaxConmutada = db.Column(db.String)
     IMaxConmutada = db.Column(db.String)
     PeriodoAlmacenamiento = db.Column(db.String)
     IdSocket = db.Column(db.String)
     FechaRegistro = db.Column(db.DateTime)
     Activo = db.Column(db.String)
     
     def __init__(self, Nombre, desc,Peso,Dimen,TMaxCon,IMaxCon,PeriodoAlma,IdSocket):       
        self.Nombre = Nombre
        self.Descripcion = desc       
        self.Peso = Peso       
        self.Dimensiones = Dimen       
        self.TMaxConmutada = TMaxCon       
        self.IMaxConmutada = IMaxCon       
        self.PeriodoAlmacenamiento = PeriodoAlma       
        self.IdSocket = IdSocket     
        self.FechaRegistro = (date.datetime.now()).strftime("%Y-%m-%d")
        self.Activo = '1' 

class Socket(db.Model):
     IdSocket = db.Column(db.Integer, primary_key=True)     
     Nombre = db.Column(db.String)
     Direccion = db.Column(db.String)
     FechaRegistro = db.Column(db.DateTime)
     Activo = db.Column(db.String)
     
     def __init__(self, Nombre, Direccion):       
        self.Nombre = Nombre
        self.Direccion = Direccion        
        self.FechaRegistro = (date.datetime.now()).strftime("%Y-%m-%d")
        self.Activo = '1' 


#schemas
class RolSchema(ma.Schema):
    class Meta:
       fields = ('IdRol','Nombre', 'Descripcion')

Rol_Schema = RolSchema()
Roles_Schema = RolSchema(many = True)

class ClienteSchema(ma.Schema):
    class Meta:      
       fields = ('IdCliente','Identificacion', 'TipoIdentificacion','Nombre','RazonSocial','Direccion','Telefono','Email','IdSocket','IdRol')

Cliente_Schema = ClienteSchema()
Clientes_Schema = ClienteSchema(many = True)

class MedidorSchema(ma.Schema):
    class Meta:
       fields = ('IdMedidor','Nombre', 'Descripcion','Peso','Dimensiones','TMaxConmutada','IMaxConmutada','PeriodoAlmacenamiento','IdSocket')
       
Medidor_Schema = MedidorSchema()
Medidores_Schema = MedidorSchema(many = True)

class SocketSchema(ma.Schema):
    class Meta:
       fields = ('IdSocket','Nombre', 'Direccion','FechaRegistro')

Socket_Schema = SocketSchema()
Sockets_Schema = SocketSchema(many = True)


#Rutas Roles
@app.route('/roles', methods = ['POST'])
def create_Rol():   
    nombre = request.json['Nombre']
    descripcion = request.json['Descripcion']
    
    new_Rol = Rol(nombre,descripcion)
    db.session.add(new_Rol)
    db.session.commit()

    return Rol_Schema.jsonify(new_Rol)
     
@app.route('/roles', methods = ['GET'])
def Get_All_Rol():   
    allRoles = Rol.query.all()
    response = Roles_Schema.dump(allRoles)
  
    return jsonify(response)
    
@app.route('/roles/<id>', methods = ['GET'])
def Get_Rol(id):   
    rol = Rol.query.get(id)
     
    return Rol_Schema.jsonify(rol)

@app.route('/roles/<id>', methods = ['DELETE'])
def Delete_Rol(id):   
    rol = Rol.query.get(id)
    db.session.delete(rol)
    db.session.commit()

    return Rol_Schema.jsonify(rol)

@app.route('/roles', methods = ['PUT'])
def Update_Rol():   
    IdRol = request.json['IdRol']
    nombre = request.json['Nombre']
    descripcion = request.json['Descripcion']
      
    _rol = Rol.query.get(IdRol)
    _rol.Nombre = nombre
    _rol.Descripcion = descripcion     
    db.session.commit()

    return Rol_Schema.jsonify(_rol)


#Rutas Clientes
@app.route('/clientes', methods = ['POST'])
def create_Cliente():     
    Identificacion = request.json['TipoIdentificacion']
    TipoIdentificacion = request.json['TipoIdentificacion']
    Nombre = request.json['Nombre']
    RazonSocial = request.json['RazonSocial']
    Direccion = request.json['Direccion']
    Telefono = request.json['Telefono']
    Email = request.json['Email']
    IdSocket = request.json['IdSocket']
    IdRol = request.json['IdRol']
 
    new_Cliente = Cliente(Identificacion,TipoIdentificacion,Nombre,RazonSocial,Direccion,Telefono,Email,IdSocket,IdRol)
    db.session.add(new_Cliente)
    db.session.commit()

    return Rol_Schema.jsonify(new_Rol)
     
@app.route('/clientes', methods = ['GET'])
def Get_All_Cliente():   
    allClientes = Cliente.query.filter_by(Activo = '1').all()
    response = Clientes_Schema.dump(allClientes)
  
    return jsonify(response)
    
@app.route('/clientes/<id>', methods = ['GET'])
def Get_Cliente(id):   
    cliente = Cliente.query.get(id)
     
    return Cliente_Schema.jsonify(cliente)

@app.route('/clientes/<id>', methods = ['DELETE'])
def Delete_Cliente(id):   
    cliente = Cliente.query.get(id)
    cliente.Activo = '0'    
    db.session.commit()

    return Cliente_Schema.jsonify(cliente)

@app.route('/clientes', methods = ['PUT'])
def Update_Clientes():   
    IdCliente = request.json['IdCliente'] 
    Identificacion = request.json['Identificacion']
    TipoIdentificacion = request.json['TipoIdentificacion']
    Nombre = request.json['Nombre']
    RazonSocial = request.json['RazonSocial']
    Direccion = request.json['Direccion']
    Telefono = request.json['Telefono']
    Email = request.json['Email']
    IdSocket = request.json['IdSocket']
    IdRol = request.json['IdRol']
 
      
    _cliente = Cliente.query.get(IdCliente)
    _cliente.Identificacion = Identificacion
    _cliente.TipoIdentificacion = TipoIdentificacion     
    _cliente.Nombre = nombre
    _cliente.RazonSocial = RazonSocial     
    _cliente.Direccion = Direccion
    _cliente.Telefono = Telefono            
    _cliente.Email = Email
    _cliente.IdSocket = IdSocket    
    _cliente.IdRol = IdRol     
    db.session.commit()

    return Cliente_Schema.jsonify(_cliente)


#Rutas Socket
@app.route('/socket', methods = ['POST'])
def create_Socket():     
    Nombre = request.json['Nombre']
    Direccion = request.json['Direccion']
     
    new_socket = Socket(Nombre, Direccion)
    db.session.add(new_socket)
    db.session.commit()

    return Socket_Schema.jsonify(new_socket)
     
@app.route('/socket', methods = ['GET'])
def Get_All_Socket():   
    allSocket = Socket.query.filter_by(Activo = '1').all()
    response = Sockets_Schema.dump(allSocket)
  
    return jsonify(response)
    
@app.route('/socket/<id>', methods = ['GET'])
def Get_Socket(id):   
    socket = Socket.query.get(id)
     
    return Socket_Schema.jsonify(socket)

@app.route('/socket/<id>', methods = ['DELETE'])
def Delete_Socket(id):   
    socket = Socket.query.get(id)
    socket.Activo = '0'    
    db.session.commit()

    return Socket_Schema.jsonify(socket)

@app.route('/socket', methods = ['PUT'])
def Update_Socket():   
    IdSocket = request.json['IdSocket']
    Nombre = request.json['Nombre']
    Direccion = request.json['Direccion']
       
    _socket = Socket.query.get(IdSocket)
    _socket.Nombre = Nombre    
    _socket.Direccion = Direccion    
    db.session.commit()

    return Socket_Schema.jsonify(_socket)



#Rutas Medidores
@app.route('/medidores', methods = ['POST'])
def create_Medidor():     
    Nombre = request.json['Nombre']
    Descripcion = request.json['Descripcion']
    Peso = request.json['Peso']
    Dimen = request.json['Dimensiones']
    TMax = request.json['TMaxConmutada']
    IMax = request.json['IMaxConmutada']
    PeriodoAlma = request.json['PeriodoAlmacenamiento']
    IdSocket = request.json['IdSocket']
          
    new_medidor = Medidor(Nombre, Descripcion,Peso,Dimen,TMax,IMax,PeriodoAlma,IdSocket)
    db.session.add(new_medidor)
    db.session.commit()

    return Medidor_Schema.jsonify(new_medidor)
     
@app.route('/medidores', methods = ['GET'])
def Get_All_Medidor():   
    allMedidor = Medidor.query.filter_by(Activo = '1').all()    
    response = Medidores_Schema.dump(allMedidor)
  
    return jsonify(response)
    
@app.route('/medidores/<id>', methods = ['GET'])
def Get_Medidor(id):   
    medidor = Medidor.query.get(id)     
    return Medidor_Schema.jsonify(medidor)

@app.route('/medidores/<id>', methods = ['DELETE'])
def Delete_Medidor(id):   
    medidor = Medidor.query.get(id)
    medidor.Activo = '0'    
    db.session.commit()

    return Medidor_Schema.jsonify(medidor)

@app.route('/medidores', methods = ['PUT'])
def Update_Medidor():   
    IdMedidor = request.json['IdMedidor']
    Nombre = request.json['Nombre']
    Descripcion = request.json['Descripcion']
    Peso = request.json['Peso']
    Dimen = request.json['Dimensiones']
    TMax = request.json['TMaxConmutada']
    IMax = request.json['IMaxConmutada']
    PeriodoAlma = request.json['PeriodoAlmacenamiento']
    IdSocket = request.json['IdSocket']

    _medidor = Medidor.query.get(IdMedidor)
    _medidor.Nombre = Nombre    
    _medidor.Descripcion = Descripcion    
    _medidor.Peso = Peso    
    _medidor.Dimensiones = Dimen    
    _medidor.TMaxConmutada = TMax    
    _medidor.IMaxConmutada = IMax    
    _medidor.PeriodoAlmacenamiento = PeriodoAlma    
    _medidor.IdSocket = IdSocket           
    db.session.commit()

    return Medidor_Schema.jsonify(_medidor)
  

#Run Server
if __name__ == '__main__':   
   app.run(debug=True, port = 4000)