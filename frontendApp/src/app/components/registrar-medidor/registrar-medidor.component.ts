import { Component, OnInit } from '@angular/core';
import { Medidores } from '../../models/medidores';
import { MedidorService } from '../../services/medidor.service';
import { SocketService } from '../../services/socket.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-registrar-medidor',
  templateUrl: './registrar-medidor.component.html'
})
export class RegistrarMedidorComponent implements OnInit {

  medidor : Medidores = new Medidores();
  swGuardo : number = 0;
  sockets : any[];
  constructor(private medidorService : MedidorService,private socketService : SocketService) { }

  ngOnInit(): void {
    this.socketService.getAllSocket()
    .subscribe(resp => {        
      this.sockets = resp;     
    })
  }

  guardarMedidor(form : NgForm){
    if(form.invalid){    
      this.swGuardo = 2;
      return 
    }    
    this.medidorService.createMedidor(this.medidor)
      .subscribe(resp => {        
        this.swGuardo = 1;
      })
  }
}
