import { Component, OnInit } from '@angular/core';
import { MedidorService } from '../../services/medidor.service';
import { Medidores } from '../../models/medidores';

@Component({
  selector: 'app-medidor',
  templateUrl: './medidor.component.html' 
})
export class MedidorComponent implements OnInit {

  medidores: Medidores[];
  medidor : Medidores;
  constructor(private servicioMedidor : MedidorService) { }  

  ngOnInit(): void {   
    this.getAllMedidores();
  }
  
  getAllMedidores(){
    this.servicioMedidor.getAllMedidores().subscribe((resultado) =>{
      this.medidores = resultado;     
     });   
  }

  deleteMedidor(id : number){     
     this.servicioMedidor.DeleteMedidor(id).subscribe();      
  }  
}

