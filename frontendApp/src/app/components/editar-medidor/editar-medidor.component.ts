import { Component, OnInit } from '@angular/core';
import { Medidores } from '../../models/medidores';
import { MedidorService } from '../../services/medidor.service';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-editar-medidor',
  templateUrl: './editar-medidor.component.html' 
})
export class EditarMedidorComponent implements OnInit {

  medidor : Medidores = new Medidores();
  swGuardo : number = 0;
  newmedidor : any;

  constructor(private medidorService : MedidorService, private _activatedRoute : ActivatedRoute) { 
    this._activatedRoute.params.subscribe( params =>{
      this.medidorService.getMedidor(params['id']).subscribe((resultado) =>{
        this.medidor = resultado;       
       });       
    });
  }

  ngOnInit(): void {}

  ActualizarrMedidor(form : NgForm){
    if(form.invalid){    
      this.swGuardo = 2;
      return 
    }    
    this.medidorService.UpdateMedidor(this.medidor)
      .subscribe(resp => {        
        this.swGuardo = 1;
      })
  }
}
