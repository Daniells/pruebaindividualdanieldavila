import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SocketService {

  constructor(private http: HttpClient) { }

  getAllSocket(): Observable<any[]> {
    return this.http.get<any>(environment.urlAPI + "/socket").pipe(
      map(res => {
        return res
      })
    );   
  }
}
