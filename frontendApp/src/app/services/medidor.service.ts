import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Medidores } from '../models/medidores';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MedidorService {

  header = new Headers();   

  constructor(private http: HttpClient) {}

  getAllMedidores(): Observable<Medidores[]> {
    return this.http.get<any>(environment.urlAPI + "/medidores").pipe(
      map(res => {
        return res as Medidores[];
      })
    );   
  }
  
  createMedidor(medidor : Medidores){
    return this.http.post(environment.urlAPI + '/medidores', medidor)
      .pipe(
          map(resp => {
            return medidor;
          })
      );
  }

  UpdateMedidor(medidor : Medidores){
    return this.http.put(environment.urlAPI + '/medidores', medidor)
      .pipe(
          map(resp => {
            return medidor;
          })
      );
  }

  DeleteMedidor(id : number){
    return this.http.delete(environment.urlAPI + '/medidores/' + id)
      .pipe(
          map(resp => {          
            return resp;           
          })
      );
  }

  getMedidor(id : string){   
    return this.http.get(environment.urlAPI + '/medidores/' + Number(id))
      .pipe(
          map(resp => {                 
            return resp as Medidores;
          })
      );
  }
}
