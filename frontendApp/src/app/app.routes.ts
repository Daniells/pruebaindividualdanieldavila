import { RouterModule, Routes } from '@angular/router';
import { ClienteComponent } from './components/cliente/cliente.component';
import { MedidorComponent } from './components/medidor/medidor.component';
import { RolesComponent } from './components/roles/roles.component';
import { SocketComponent } from './components/socket/socket.component';
import { RegistrarMedidorComponent } from './components/registrar-medidor/registrar-medidor.component';
import { EditarMedidorComponent } from './components/editar-medidor/editar-medidor.component';

const APP_ROUTES : Routes = [
{path : 'cliente', component : ClienteComponent },
{path : 'medidor', component : MedidorComponent },
{path : 'roles', component : RolesComponent },
{path : 'socket', component : SocketComponent },
{path : 'RegistrarMedidor', component : RegistrarMedidorComponent },
{path : 'ActualizarMedidor/:id', component : EditarMedidorComponent },
{path : '**', pathMatch : 'full', redirectTo:'medidor' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, {useHash : true});