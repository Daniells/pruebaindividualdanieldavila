import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

//Rutas
import { APP_ROUTING } from './app.routes';

//servicios
import { MedidorService } from './services/medidor.service';
import { SocketService } from './services/socket.service';

import { AppComponent } from './app.component';
import { MedidorComponent } from './components/medidor/medidor.component';
import { RolesComponent } from './components/roles/roles.component';
import { SocketComponent } from './components/socket/socket.component';
import { ClienteComponent } from './components/cliente/cliente.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import { RegistrarMedidorComponent } from './components/registrar-medidor/registrar-medidor.component';
import { EditarMedidorComponent } from './components/editar-medidor/editar-medidor.component';

@NgModule({
  declarations: [
    AppComponent,
    MedidorComponent,
    RolesComponent,
    SocketComponent,
    ClienteComponent,
    NavbarComponent,
    FooterComponent,
    RegistrarMedidorComponent,
    EditarMedidorComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    APP_ROUTING
  ],
  providers: [
    MedidorService,
    SocketService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
