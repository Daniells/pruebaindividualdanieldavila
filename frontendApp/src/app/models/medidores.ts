export class Medidores {  

    Descripcion: string; 
    Dimensiones: string; 
    IMaxConmutada: string; 
    IdMedidor: number; 
    IdSocket: number; 
    Nombre: string; 
    PeriodoAlmacenamiento: string; 
    Peso: string; 
    TMaxConmutada: string;      
}